package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Category;
import com.example.demo.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	public List<Category> getAllCategories() {
		return categoryRepository.findAll();
	}

	public List<Category> getCategoriesByUserId(int id) {
		return categoryRepository.findByUserIdOrderByName(id);
	}

	public void saveCategory(Category category) {
		categoryRepository.save(category);
	}

	public void deleteCategory(int id) {
		categoryRepository.deleteById(id);
	}
}
