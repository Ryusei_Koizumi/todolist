package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.utils.CipherUtil;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User getUser(String account, String password) {

		String encPassword = CipherUtil.encrypt(password);

		return userRepository.findByAccountAndPassword(account, encPassword);
	}

	public void addAccount(User user) {

		String password = user.getPassword();
		String encPassword = CipherUtil.encrypt(password);
		user.setPassword(encPassword);

		userRepository.save(user);
	}

	public User getUserByAccount(String account) {
		return userRepository.findByAccount(account);
	}
}