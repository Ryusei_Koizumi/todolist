package com.example.demo.service;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.TodoCategory;
import com.example.demo.repository.TodoCategoryRepository;

@Service
@Transactional
public class TodoCategoryService {

	@Autowired
	TodoCategoryRepository todoCategoryRepository;

	public List<TodoCategory> getAllTodoCategories() {
		return todoCategoryRepository.getAllTodosWithCategories();
	}

	public List<TodoCategory> findTodoCategories(int id, Date today) {
		return todoCategoryRepository.findByUserIdAndScheduleDate(id, today);
	}

	public List<TodoCategory> findUndoneTodos(int id) {
		return todoCategoryRepository.findByUserId(id);
	}

}
