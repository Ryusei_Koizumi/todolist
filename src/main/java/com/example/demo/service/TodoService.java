package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Todo;
import com.example.demo.repository.TodoRepository;

@Service
public class TodoService {

	@Autowired
	TodoRepository todoRepository;

	public void saveTodo(Todo newTodo) {
		todoRepository.save(newTodo);
	}

	public List<Todo> getAllTodos() {
		return todoRepository.findAll();
	}

	public Todo getTodoById(Integer id) {
		return (Todo)todoRepository.findById(id).orElse(null);
	}

	public void deleteTodo(Integer id) {
		todoRepository.deleteById(id);
	}
}
