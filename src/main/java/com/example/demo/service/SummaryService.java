package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.TodoSummary;
import com.example.demo.repository.TodoSummaryRepository;

@Service
public class SummaryService {

	@Autowired
	TodoSummaryRepository todoSummaryRepository;

	public List<TodoSummary> getTodoSummaries() {
		return todoSummaryRepository.getTodosSummary();
	}

	public List<TodoSummary> geTodoSummariesById(int id){
		return todoSummaryRepository.findByUserId(id);
	}
}
