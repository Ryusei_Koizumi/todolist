package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TodoSummary {

	@Id
	@Column
	private int id;

	@Column(name = "user_id")
	private int userId;

	@Column
	private String todo;

	@Column(name = "genre_name")
	private String genre;

	@Column(name = "ideal_time")
	private String idealTime;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "actual_time")
	private String actualTime;

	@Column(name = "schedule_date")
	private Date scheduleDate;

	@Column(name = "delta_time")
	private String deltaTime;

	public String getDeltaTime() {
		return deltaTime;
	}

	public void setDeltaTime(String deltaTime) {
		this.deltaTime = deltaTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getTodo() {
		return todo;
	}

	public void setTodo(String todo) {
		this.todo = todo;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getIdealTime() {
		return idealTime;
	}

	public void setIdealTime(String idealTime) {
		this.idealTime = idealTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

}
