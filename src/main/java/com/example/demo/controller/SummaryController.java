package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.TodoSummary;
import com.example.demo.entity.User;
import com.example.demo.service.SummaryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class SummaryController {

	@Autowired
	SummaryService summaryService;

	@Autowired
	HttpSession session;

	@GetMapping("/summary")
	@ResponseBody
	public ModelAndView summary() {

		ModelAndView mav = new ModelAndView();

		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		mav.setViewName("summary");

		return mav;
	}

	@GetMapping("/search")
    @ResponseBody
	public String search() {
		User loginUser = (User)session.getAttribute("loginUser");
		int id = loginUser.getId();
		List<TodoSummary> todoSummaries = summaryService.geTodoSummariesById(id);
        return getJson(todoSummaries);
	}

	private String getJson(List<TodoSummary> todoSummaries) {
        String s = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            s = objectMapper.writeValueAsString(todoSummaries);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return s;
    }

}
