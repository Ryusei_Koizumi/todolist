package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Category;
import com.example.demo.entity.User;
import com.example.demo.service.CategoryService;

@RestController
public class CategoryController {

	@Autowired
	HttpSession session;

	@Autowired
	CategoryService categoryService;

	@PostMapping("/category")
	public ModelAndView createCategory(@ModelAttribute("formModel") Category categoryForm) {

		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(categoryForm, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			return new ModelAndView("redirect:/todolist#add-category");
		}

		Category category = new Category();
		category.setName(categoryForm.getName());

		User loginUser = (User) session.getAttribute("loginUser");
		category.setUserId(loginUser.getId());

		categoryService.saveCategory(category);

		return new ModelAndView("redirect:/todolist#add-category");
	}

	@DeleteMapping("/category/delete/{id}")
	public ModelAndView deleteCategory(@PathVariable int id) {

		categoryService.deleteCategory(id);

		return new ModelAndView("redirect:/todolist#add-category");
	}

	private boolean isValid(Category categoryForm, List<String> errorMessages) {
		String category = categoryForm.getName();

		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリーを入力してください。");
		}

		if (errorMessages.size() > 0) {
			return false;
		}

		return true;
	}
}
