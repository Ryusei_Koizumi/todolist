package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
public class LoginController {

	@Autowired
	HttpSession session;

	@Autowired
	UserService userService;

	@GetMapping("/login")
	public ModelAndView top() {

		ModelAndView mav = new ModelAndView();

		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		session.removeAttribute("errorMessages");

		mav.setViewName("/login");
		return mav;
	}

	@PostMapping("/login")
	public ModelAndView login(@ModelAttribute("formModel") User user) {

		ModelAndView mav = new ModelAndView();

		String account = user.getAccount();
		String password = user.getPassword();

		User loginUser = userService.getUser(account, password);
		if (loginUser == null) {
			mav.addObject("errorMessages", "アカウントまたはパスワードが誤っています");
			mav.addObject("account", account);
			mav.setViewName("/login");
			return mav;
		}

		session.setAttribute("loginUser", loginUser);
		return new ModelAndView("redirect:/todolist");
	}

	@RequestMapping("/logout")
	public ModelAndView logout() {

		session.invalidate();

		return new ModelAndView("redirect:/login");

	}

}
