package com.example.demo.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Category;
import com.example.demo.entity.TodoCategory;
import com.example.demo.entity.User;
import com.example.demo.service.CategoryService;
import com.example.demo.service.TodoCategoryService;

@SpringBootApplication
@RestController
public class HomeController {

	@Autowired
	HttpSession session;

	@Autowired
	CategoryService categoryService;

	@Autowired
	TodoCategoryService todoCategoryService;

	/**
	 * ホーム画面表示
	 * @return ログインユーザー情報
	 * 			カテゴリー一覧
	 * 			当日のログインユーザーのTodo一覧
	 */
	@GetMapping("/todolist")
	public ModelAndView home() {

		ModelAndView mav = new ModelAndView();

		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		int id = loginUser.getId();

		List<Category> categories = categoryService.getCategoriesByUserId(id);
		mav.addObject("genres", categories);

		List<TodoCategory> todoCategories = todoCategoryService.findUndoneTodos(id);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		for (TodoCategory todoCategory : todoCategories) {
			String schedule = df.format(todoCategory.getScheduleDate());
			Date formattedDate = java.sql.Date.valueOf(schedule);;
			todoCategory.setScheduleDate(formattedDate);
		}

		mav.addObject("todoCategories", todoCategories);

		mav.setViewName("home");
		return mav;
	}

	/**
	 * 日付によるToDo検索
	 * @return ログインユーザー情報
	 * 			カテゴリー一覧
	 * 			指定日時のログインユーザーのTodo一覧
	 */
	@GetMapping("/search-by-date")
	public ModelAndView searchByDate(@RequestParam Date scheduleDate) throws ParseException {

		ModelAndView mav = new ModelAndView();

		User loginUser = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		List<Category> categories = categoryService.getAllCategories();
		mav.addObject("genres", categories);

		Date schedule = scheduleDate;
		mav.addObject("scheduleDate", scheduleDate);

		int id = loginUser.getId();
		List<TodoCategory> todoCategories = todoCategoryService.findTodoCategories(id, schedule);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		for (TodoCategory todoCategory : todoCategories) {
			String date = df.format(todoCategory.getScheduleDate());
			Date formattedDate = java.sql.Date.valueOf(date);
			todoCategory.setScheduleDate(formattedDate);
		}
		mav.addObject("todoCategories", todoCategories);

		mav.setViewName("home");
		return mav;
	}
}