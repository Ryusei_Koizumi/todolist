package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
public class SignupController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	@GetMapping("/signup")
	public ModelAndView signup() {
		return new ModelAndView("/signup");
	}

	@PostMapping("/signup")
	public ModelAndView addAccount(@ModelAttribute("formModel") User user) {

		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(user, errorMessages)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("account", user.getAccount());
			mav.addObject("name", user.getName());
			mav.setViewName("signup");
			return mav;
		}
		session.setAttribute("errorMessages", "アカウントが新しく登録されました。");
		userService.addAccount(user);

		return new ModelAndView("redirect:/login");
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String account = user.getAccount();
		String name = user.getName();
		String password = user.getPassword();

		User duplicatedUser = userService.getUserByAccount(account);

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください。");
		}
		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください。");
		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください。");
		}
		if (duplicatedUser != null) {
			System.out.println(duplicatedUser);
			errorMessages.add("アカウントが重複しているため、登録できません。");
		}
		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}