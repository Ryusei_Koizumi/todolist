package com.example.demo.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Todo;
import com.example.demo.entity.User;
import com.example.demo.form.NewTodoForm;
import com.example.demo.service.TodoService;

@RestController
public class TodoController {

	@Autowired
	TodoService todoService;

	@Autowired
	HttpSession session;

	@PutMapping("/todo/start/{id}")
	public ModelAndView startTodo(@PathVariable int id) {

		Todo startTodo = todoService.getTodoById(id);

		Timestamp startStopWatch = new Timestamp(System.currentTimeMillis());
		startTodo.setStartTime(startStopWatch);
		todoService.saveTodo(startTodo);

		return new ModelAndView("redirect:/todolist");
	}

	@PutMapping("/todo/end/{id}")
	public ModelAndView endTodo(@PathVariable int id) {

		Todo todo = todoService.getTodoById(id);

		Timestamp startStopWatch = (Timestamp)todo.getStartTime();
		Timestamp stopStopWatch = new Timestamp(System.currentTimeMillis());

		final double MILLISECONDS_TO_HOUR = 1000 * 60 * 60;
		double deltaTime = (stopStopWatch.getTime() - startStopWatch.getTime()) / MILLISECONDS_TO_HOUR;
		double deltaHour = Math.floor(deltaTime * 10) / 10;

		String endTime = String.valueOf(deltaHour);
		todo.setActualTime(endTime);

		Double ideal = Double.parseDouble(todo.getIdealTime());
		Double actual = Double.parseDouble(todo.getActualTime());
		String delta = String.valueOf(Math.floor((actual-ideal) * 10) / 10);
		todo.setDeltaTime(delta);

		todoService.saveTodo(todo);

		return new ModelAndView("redirect:/todolist");
	}

	@PostMapping("/todo/add")
	public ModelAndView addTodo(@ModelAttribute("formModel") @Valid NewTodoForm form) {

		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(form, errorMessages)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("home");
			return mav;
		}

		Todo createTodo = toTodo(form);
		todoService.saveTodo(createTodo);

		return new ModelAndView("redirect:/todolist");
	}

	@DeleteMapping("/todo/delete/{id}")
	public ModelAndView deleteTodo(@PathVariable int id) {

		todoService.deleteTodo(id);

		return new ModelAndView("redirect:/todolist");
	}

	@PutMapping("/todo/quit/{id}")
	public ModelAndView quitTodo(@PathVariable int id) {

		Todo quitTodo = todoService.getTodoById(id);
		quitTodo.setStartTime(null);
		todoService.saveTodo(quitTodo);

		return new ModelAndView("redirect:/todolist");
	}

	private Todo toTodo(NewTodoForm form) {

		Todo newTodo = new Todo();

		User loginUser = (User) session.getAttribute("loginUser");
		newTodo.setUserId(loginUser.getId());

		Date scheduleDate = Date.valueOf(form.getScheduleDate());
		newTodo.setScheduleDate(scheduleDate);

		newTodo.setText(form.getText());
		newTodo.setGenreId(form.getGenreId());
		newTodo.setIdealTime(form.getIdealTime());

		return newTodo;
	}

	private boolean isValid(NewTodoForm form, List<String> errorMessages) {

		String text = form.getText();
		String idealTime = form.getIdealTime();
		int category = form.getGenreId();
		String actualNumberRegex = "\\d+(?:\\.\\d+)?";

		if (StringUtils.isBlank(text)) {
			errorMessages.add("TODOを入力してください。");
		}
		if (StringUtils.isBlank(idealTime)) {
			errorMessages.add("想定時間を入力してください。");
		} else if (idealTime.matches(actualNumberRegex)) {
			errorMessages.add("想定時間は半角実数のみ入力可能です。");
		}
		if (!(category >= 1)) {
			errorMessages.add("カテゴリーを選択してください。");
		}

		return true;
	}
}