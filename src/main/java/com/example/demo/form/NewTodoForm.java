package com.example.demo.form;

public class NewTodoForm {

	private int genreId;
	private String scheduleDate;
	private String text;
	private String idealTime;

	public int getGenreId() {
		return genreId;
	}

	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}

	public String getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIdealTime() {
		return idealTime;
	}

	public void setIdealTime(String idealTime) {
		this.idealTime = idealTime;
	}

}
