package com.example.demo.filter;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.demo.entity.User;

public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest)request).getSession();

        String requestUri = ((HttpServletRequest) request).getRequestURI();
        User loginUser = (User) session.getAttribute("loginUser");

        String loginErrorMessages = "ログインしてください";

        if (loginUser != null || requestUri.contains("login") || requestUri.contains("signup")) {
            chain.doFilter(request, response);
        } else {
        	HttpServletResponse httpResponse = (HttpServletResponse) response;
        	session.setAttribute("errorMessages", loginErrorMessages);
        	httpResponse.sendRedirect("./login");
        }
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
