package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.TodoSummary;

@Repository
public interface TodoSummaryRepository extends JpaRepository<TodoSummary, Integer>{

	@Query(value = "SELECT todos.id AS id, todos.user_id AS user_id, todos.todo AS todo, genres.name AS genre_name, todos.ideal_time AS ideal_time, todos.start_time AS start_time, todos.actual_time AS actual_time, todos.delta_time AS delta_time, todos.schedule_date AS schedule_date FROM todos LEFT JOIN genres ON todos.genre_id = genres.id LEFT JOIN users ON users.id = todos.user_id ORDER BY todos.schedule_date ASC", nativeQuery = true)
	List<TodoSummary> getTodosSummary();

	@Query(value = "SELECT todos.id AS id, todos.user_id AS user_id, todos.todo AS todo, genres.name AS genre_name, todos.ideal_time AS ideal_time, todos.start_time AS start_time, todos.actual_time AS actual_time, todos.delta_time AS delta_time, todos.schedule_date AS schedule_date FROM todos LEFT JOIN genres ON todos.genre_id = genres.id LEFT JOIN users ON users.id = todos.user_id WHERE todos.user_id = :id ORDER BY todos.schedule_date ASC", nativeQuery = true)
	List<TodoSummary> findByUserId(int id);

}
