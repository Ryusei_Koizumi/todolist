package com.example.demo.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.TodoCategory;

@Repository
public interface TodoCategoryRepository extends JpaRepository<TodoCategory, Integer>{

	@Query(value = "SELECT todos.id AS id, todos.start_time AS start_time, todos.actual_time as actual_time, todos.delta_time AS delta_time, todos.todo AS todo, todos.user_id AS user_id, genres.name AS category_name, todos.ideal_time AS ideal_time, todos.schedule_date AS schedule_date, todos.created_date AS created_date FROM todos INNER JOIN genres ON (todos.genre_id = genres.id) ORDER BY actual_time DESC NULLS FIRST, start_time ASC NULLS LAST, created_date ASC;", nativeQuery = true)
	List<TodoCategory> getAllTodosWithCategories();

	@Query(value = "SELECT todos.id AS id, todos.start_time AS start_time, todos.actual_time as actual_time, todos.delta_time AS delta_time, todos.todo AS todo, todos.user_id AS user_id, genres.name AS category_name, todos.ideal_time AS ideal_time, todos.schedule_date AS schedule_date, todos.created_date AS created_date FROM todos INNER JOIN genres ON todos.genre_id = genres.id WHERE todos.user_id = :id AND todos.schedule_date = :scheduleDate ORDER BY actual_time DESC NULLS FIRST, start_time ASC NULLS LAST, created_date ASC", nativeQuery = true)
	List<TodoCategory> findByUserIdAndScheduleDate(int id, Date scheduleDate);

	@Query(value = "SELECT todos.id AS id, todos.start_time AS start_time, todos.actual_time as actual_time, todos.delta_time AS delta_time, todos.todo AS todo, todos.user_id AS user_id, genres.name AS category_name, todos.ideal_time AS ideal_time, todos.schedule_date AS schedule_date, todos.created_date AS created_date FROM todos INNER JOIN genres ON todos.genre_id = genres.id WHERE todos.user_id = :id AND todos.delta_time IS NULL ORDER BY actual_time DESC NULLS FIRST, start_time ASC NULLS LAST, schedule_date ASC", nativeQuery = true)
	List<TodoCategory> findByUserId(int id);
}
