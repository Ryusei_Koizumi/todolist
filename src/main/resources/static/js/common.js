/**
 *
 */

// 本日の日時を表示
window.onload = function() {
	var date = new Date()

	var yyyy = date.getFullYear()
	var mm = ("0" + (date.getMonth() + 1)).slice(-2)
	var dd = ("0" + date.getDate()).slice(-2)

	var dateToday = document.getElementsByClassName('today')

	for (var today of dateToday){
		today.value = yyyy + '-' + mm + '-' + dd
	}
}