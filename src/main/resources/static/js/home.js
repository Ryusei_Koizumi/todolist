/**
 *
 */

document.addEventListener("DOMContentLoaded", function(){
    document.getElementById('searchBtn').addEventListener('click', function(){

    	const errorMessage = document.getElementById('errorMessage')
    	const form = document.getElementById('searchByDateForm')
    	const btn = document.getElementById('searchBtn')
    	const valNull = null
    	const nothingInside = ''
		let date = document.getElementById('searchedDate').value

    	console.log(date)

    	if (errorMessage != valNull) {
    		alert(errorMessage.value)
    	} else if (date === nothingInside) {
    		alert('日付を入力してください。')
    	} else {
    		form.action = "/search-by-date"
    		form.method = "get"
    		form.submit()
    	}
    });
}, false);

document.addEventListener("DOMContentLoaded", function(){
    document.getElementById('addTodoBtn').addEventListener('click', function(){

    	const errorMessage = document.getElementById('errorMessage')
    	const form = document.getElementById('addTodo')
    	const btn = document.getElementById('addTodoBtn')
    	const valNull = null
    	const nothingInside = ''
    	let scheduleDate = document.getElementById('scheduleDate').value
    	let text = document.getElementById('text').value
    	let category = document.getElementById('category').value
    	let idealTime = document.getElementById('idealTime').value

    	console.log(scheduleDate)
    	console.log(category)

    	if (errorMessage != valNull) {
    		alert(errorMessage.value)
    	} else if (!scheduleDate) {
    		alert('日付を入力してください。')
    	} else if (!text.match(/\S/g)) {
    		alert('ToDoを入力してください。')
    	} else if (category === nothingInside) {
    		alert('カテゴリーを入力してください。')
    	} else if (!idealTime.match(/\S/g)) {
    		alert('想定時間を入力してください。')
    	} else if (!idealTime.match(/^([1-9]\d*|0)(\.\d{1})?$/)) {
    		alert('想定時間は小数点第一位までの半角実数を入力してください。')
    	} else {
    		form.action = "/todo/add"
    		form.method = "post"
    		form.submit();
    		alert('TODOを追加しました。')
    	}
    });
}, false);

document.addEventListener("DOMContentLoaded", function(){
    document.getElementById('addCategoryBtn').addEventListener('click', function(){

    	const errorMessage = document.getElementById('errorMessage')
    	const form = document.getElementById('addCategory')
    	const btn = document.getElementById('addCategoryBtn')
    	const valNull = null
    	const nothingInside = ''
    	let category = document.getElementById('newCategory').value

    	if (errorMessage != valNull) {
    		alert(errorMessage.value)
    	} else if (!category.match(/\S/g)) {
    		alert('カテゴリーを入力してください。')
    	} else {
    		form.action = "/category"
    		form.method = "post"
    		form.submit()
    		alert('カテゴリーを追加しました。')
    	}
    });
}, false);

/*
document.addEventListener("DOMContentLoaded", function(){
    document.getElementById('todoText').addEventListener('click', function(){
    	let todoId = document.getElementById('todoCategoryId').value
    	console.log(todoId)

    });
}, false);

*/
