/**
 *
 */

$(document).ready(function() {
    $('#fav-table').tablesorter();
});

$.ajax({
	url : "/search",
	dataType : "text",
	type : "GET"
// Ajaxが正常終了した場合
}).done(function(data, textStatus, jqXHR) {
	// 該当するデータが無かった場合
	if (!data) {
		alert("該当するデータはありませんでした");
		return;
	}
	const todoSummaryList = JSON.parse(data)

	// HTMLを初期化
	$("table.tbl tbody").html("");

	// HTMLを生成
	for(var index in todoSummaryList){

	var d = new Date(todoSummaryList[index].scheduleDate);
	var formattedDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();

	let actualTime = todoSummaryList[index].actualTime
	let deltaTime = todoSummaryList[index].deltaTime
	let genre = todoSummaryList[index].genre

	if (actualTime != null) {
		actualTime = actualTime + 'H'
	}else{
		actualTime = '-'
	}

	if (deltaTime != null) {
		deltaTime = deltaTime + 'H'
	}else{
		deltaTime = '-'
	}

	if (genre === null) {
		genre = '-削除されました-'
	}

	$('<tr>'+
	'<td class="align-middle">' + formattedDate + '</td>'+
	'<td class="align-middle">' + todoSummaryList[index].todo + '</td>'+
	'<td class="align-middle">' + genre + '</td>'+
	'<td class="align-middle text-center">' + todoSummaryList[index].idealTime + 'H</td>'+
	'<td class="align-middle text-center">' + actualTime + '</td>'+
	'<td class="align-middle text-center">' + deltaTime + '</td>'+
	'</tr>').appendTo('table.tbl tbody');
	}
});




